// posts.js
import axios from 'axios'

const API_URL = window.URL_API

export const apiAuth = {
  async login(email:string, password:any) {
    const formData = {
      email: email,
      password: password
    };
    const response = await axios.post(API_URL + '/api/login', formData)
    localStorage.setItem('accessToken', response.data.access_token);
    return response.data;
  },
  async register(formData) {
    const response = await axios.post(API_URL + '/api/register', formData,{
      headers: {
        'Accept': 'application/json' // Assuming you are sending JSON data
      }
    })
    return response.data;
  }
}