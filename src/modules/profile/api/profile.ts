// posts.js
import axios from 'axios'
import { useUserStore } from '../../../stores/user'

const API_URL = window.URL_API
const accessToken = localStorage.getItem('accessToken');

export const apiProfile = {
  async updateProfile(formData:FormData) {
    const userStore = useUserStore()
    const response = await axios.post(API_URL + '/api/profile', formData, {
      headers: {
        'Authorization': 'Bearer ' + userStore.accessToken,
        'Accept': 'application/json' // Assuming you are sending JSON data
      }
    })
    return response.data;
  }
}