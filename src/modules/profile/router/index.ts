export default [
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/ProfileForm.vue'),
    meta: {
      requiresAuth: true
    }
  }
]
