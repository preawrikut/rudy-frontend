export default [
  {
    path: '/profiles',
    name: 'Profiles',
    component: () => import('../views/Profiles.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Profiles.vue'),
    meta: {
      requiresAuth: true
    }
  }
]
