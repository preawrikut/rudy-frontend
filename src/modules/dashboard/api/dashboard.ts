// posts.js
import axios from 'axios'

const API_URL = window.URL_API

export const apiDashboard = {
  async getProfiles() {
    const response = await axios.get(API_URL + '/api/profiles')
    return response.data;
  }
}