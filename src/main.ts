import { createApp } from 'vue'
import { createPinia } from 'pinia'
import BootstrapVue3 from 'bootstrap-vue-3';

import App from './App.vue'
import router from './router'
// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'

import './assets/main.css'
import './assets/main.css'

// Set global variable
declare global {
  interface Window {
    URL_API: string;
    BASE_URL: string;
  }
}

window.URL_API = 'http://localhost:8000';

const app = createApp(App)
app.config.globalProperties.url_api = 'http://localhost:8000/'
app.use(createPinia())
app.use(router)
app.use(BootstrapVue3);
app.mount('#app')
