import { defineStore } from 'pinia'
import { useLocalStorage } from '@vueuse/core'

export const useUserStore = defineStore('user', {
  state: () => ({
    isLoggedIn: useLocalStorage('isLoggedIn', false),
    userData: useLocalStorage('userData', {}),
    accessToken: useLocalStorage('accessToken', {}),
  }),
  actions: {
    login(data:any) {
      this.isLoggedIn = true;
      this.userData = data.user;
      this.accessToken = data.access_token;
    },
    logout() {
      this.isLoggedIn = false;
      this.userData = {};
      this.accessToken = {};
    },
    updateProfile(user:any) {
      this.userData = user;
    }
  },
});