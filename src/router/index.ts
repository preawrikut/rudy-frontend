import { createRouter, createWebHistory } from 'vue-router'
import RouterProfile from '../modules/profile/router'
import RouterUserAuth from '../modules/userAuth/router'
import RouterDashboard from '../modules/dashboard/router'
import { useUserStore } from '../stores/user';

const routes = [
  ...RouterProfile,
  ...RouterUserAuth,
  ...RouterDashboard,
  ...RouterProfile
];
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const userStore = useUserStore()
  const isAuthenticated = userStore.isLoggedIn;
  
  if (to.matched.some(route => route.meta.requiresAuth)) {
    
    // If the route requires authentication and the user is not authenticated, redirect to the login page
    if (!isAuthenticated) {
      next('/login');
    } else {
      next()
    }

  } else {
    next()
  }
});


export default router
